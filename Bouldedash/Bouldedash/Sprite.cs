﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouldedash
{
    class Sprite
    {
        // -------------------
        // Data
        // -------------------
        protected Vector2 position;
        protected Texture2D texture;
        protected bool visible = true;


        // ------------------
        // Behaviour
        // ------------------
        public Sprite(Texture2D newTexture)
        {
            texture = newTexture;
        }
        

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(texture, position, Color.White);
            }
        }

        public bool GetVisible()
        {
            return visible;
        }
       

        public void SetVisible(bool newVisible)
        {
            visible = newVisible;
        }
      

      
        public void SetPosition(Vector2 newPostion)
        {
            position = newPostion;
        }

        public virtual Rectangle GetBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)texture.Width, (int)texture.Height);
        }

        public void DrawBounds(SpriteBatch spritebatch, GraphicsDevice Graphics)
        {
            Rectangle bounds = GetBound();

            Texture2D boundsTextutre = new Texture2D(Graphics, bounds.Width, bounds.Height);

            Color[] colorData = new Color[bounds.Width * bounds.Height];
            for (int i = 0; i < colorData.Length; i++)
            {
                colorData[i] = Color.White;
            }
            boundsTextutre.SetData(colorData);

            spritebatch.Draw(boundsTextutre, position, Color.White);
        }
    }
}
}
