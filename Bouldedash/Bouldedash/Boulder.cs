﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouldedash
{
    class Boulder : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;

        private Vector2 direction;

        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 2f;

        //--------------------
        //behaviour
        //--------------------
        public Boulder(Texture2D newTexture, Level newLevel)
           : base(newTexture)
        {  
            ourLevel = newLevel;
        }

        public bool TryPush(Vector2 Direction)
        {
            // New position the boulder will be in after the push
            Vector2 newGridPos = GetTilePosition() + Direction;



            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPos(newGridPos);


            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            //if target is a Boulder return false
            if (tileInDirection != null && tileInDirection is Boulder)
            {
                return false;
            }

            // If the target tile dirt return false
            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }
         

            return ourLevel.TryMoveTile(this, newGridPos);
        }

        public bool TryFall()
        {
            Vector2 gravity = new Vector2();
            gravity.Y = 1f;
            Vector2 newPos = GetTilePosition() + gravity;
            Tile tileInPos = ourLevel.GetTileAtPos(newPos);

            if (tileInPos!= null, tileInPos || tileInPos)
        }

    }
}
