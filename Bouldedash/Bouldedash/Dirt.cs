﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouldedash
{
    class Dirt : Tile
    {
        public Dirt(Texture2D newTexture)
           : base(newTexture)
        {
        }
    }
}
