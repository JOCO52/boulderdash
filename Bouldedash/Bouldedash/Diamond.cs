﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouldedash
{
    class Diamond : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;

        private Vector2 direction;

        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 2f;

        //-------------------
        //Behvaviour
        //-------------------
        public Diamond(Texture2D newTexture, Level newLevel)
           : base(newTexture)
        {
            ourLevel = newLevel;
        }
    }
}
