﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouldedash
{
    class Tile : Sprite
    {
        // ------------------
        // data
        // ------------------
        private Vector2 tilePosition;

        private const int TILE_SIZE = 100;


        // ------------------
        // Behaviour
        // ------------------

        public Tile(Texture2D newTexture)
            : base(newTexture)
        {
        }

        public void SetTilePosition(Vector2 newTilePosition)
        {

            tilePosition = newTilePosition;
            //set position based on tile position
            // multiply tile position by tile size 
            SetPosition(tilePosition * TILE_SIZE);

        }

        public Vector2 GetTilePosition()
        {
            return tilePosition;
        }

        public virtual void Update(GameTime gameTime)
        {
            //Blank -to be impleneted by derived or child classes
        }
    }
}
